/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * This file is part of Nautilus Kill MacOSx Metadata.
 * https://gitlab.gnome.org/glerro/nautilus-kill-macosx-metadata
 *
 * nautilus-kill-macosx-metadata-module.c
 *
 * Copyright (c) 2022-2023 Gianni Lerro {glerro} ~ <glerro@pm.me>
 *
 * Nautilus Kill MacOSx Metadata is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Nautilus Kill MacOSx Metadata is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Nautilus Kill MacOSx Metadata. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022-2023 Gianni Lerro <glerro@pm.me>
 */

#include "nautilus-kill-macosx-metadata.h"
#include "nautilus-kill-macosx-metadata-config.h"

#include <glib.h>
#include <nautilus-extension.h>

/* ----------------------------------------------------------------------------
 * Nautilus Callbacks
 */

void
nautilus_module_initialize (GTypeModule *module)
{
    g_debug ("Initializing nautilus-kill-macosx-metadata module\n");

    nautilus_kill_macosx_metadata_register_type (module);

    g_debug ("nautilus-kill-macosx-metadata module initialized\n");
}

void
nautilus_module_shutdown (void)
{
    g_debug ("nautilus-kill-macosx-metadata module shutdown\n");
}

void
nautilus_module_list_types (const GType **types,
                            int          *num_types)
{
    static GType provider_type_list[1];

    provider_type_list[0] = NAUTILUS_KILL_MACOSX_METADATA_TYPE;
    *types = provider_type_list;

    *num_types = G_N_ELEMENTS (provider_type_list);
}

