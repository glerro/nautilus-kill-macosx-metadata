/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * This file is part of Nautilus Kill MacOSx Metadata.
 * https://gitlab.gnome.org/glerro/nautilus-kill-macosx-metadata
 *
 * nautilus-kill-macosx-metadata.c
 *
 * Copyright (c) 2022-2023 Gianni Lerro {glerro} ~ <glerro@pm.me>
 *
 * Nautilus Kill MacOSx Metadata is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Nautilus Kill MacOSx Metadata is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Nautilus Kill MacOSx Metadata. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022-2023 Gianni Lerro <glerro@pm.me>
 */

#include "config.h"

#include "nautilus-kill-macosx-metadata.h"

#include <glib.h>
#include <glib/gstdio.h>
#include <nautilus-extension.h>

static GType nautilus_kill_macosx_metadata_type = 0;

static GObjectClass *parent_class;

static
void delete_directory_tree(gchar *directory_name)
{
    GDir *directory;
    const gchar *directory_child;
    GError *error = NULL;

    directory = g_dir_open (directory_name, 0, &error);

    if (directory != NULL)
    {
        while ((directory_child = g_dir_read_name (directory)) != NULL)
        {
            gchar *child_filename;

            child_filename = g_build_filename (directory_name, directory_child, NULL);

            if (g_file_test (child_filename, G_FILE_TEST_IS_DIR))
            {
                delete_directory_tree (child_filename);

                if (g_rmdir (child_filename) == -1)
                {
                    g_printerr ("Unable to remove the directory %s: %s\n", child_filename, g_strerror (errno));
                }
            }
            else
            {
                if (g_unlink (child_filename) == -1)
                {
                    g_printerr ("Unable to unlink %s: %s\n", child_filename, g_strerror (errno));
                }
            }

            g_free (child_filename);
        }

        g_dir_close (directory);
    }
    else
    {
        g_printerr ("Unable to open the directory %s - %s\n", directory_name, error->message);
        g_error_free (error);
    }
}

static NautilusOperationResult
nautilus_kill_macosx_metadata_update_file_info (NautilusInfoProvider     *info_provider,
                                                NautilusFileInfo         *nautilus_file,
                                                GClosure                 *update_complete,
                                                NautilusOperationHandle **operation_handle)
{
    gchar *filename;

    filename = nautilus_file_info_get_name (nautilus_file);

    /* Hidden ._.DS_Store or .DS_Store Files */
    if (g_str_equal (filename, "._.DS_Store") || g_str_equal (filename, ".DS_Store"))
    {
        gchar *uri = nautilus_file_info_get_uri (nautilus_file);
        gchar *filename = g_filename_from_uri (uri, NULL, NULL);
        g_free (uri);

        if (g_unlink (filename) != -1)
        {
            nautilus_file_info_add_emblem (nautilus_file, "unreadable");
            nautilus_file_info_invalidate_extension_info (nautilus_file);
        }
        else
        {
            g_free (filename);
            return NAUTILUS_OPERATION_FAILED;
        }

        g_free (filename);
    }

    /* __MACOSX Folders */
    if (g_str_equal (filename, "__MACOSX"))
    {
        if (nautilus_file_info_is_directory (nautilus_file))
        {
            gchar *uri = nautilus_file_info_get_uri (nautilus_file);
            gchar *filename = g_filename_from_uri (uri, NULL, NULL);
            g_free (uri);
            delete_directory_tree (filename);
            if (g_rmdir (filename) == -1)
            {
                return NAUTILUS_OPERATION_FAILED;
            }

            g_free (filename);
        }
    }

    return NAUTILUS_OPERATION_COMPLETE;
}

static void
nautilus_kill_macosx_metadata_type_info_provider_iface_init (NautilusInfoProviderInterface *iface,
                                                             gpointer                      *iface_data)
{
    iface->update_file_info = nautilus_kill_macosx_metadata_update_file_info;
}

static void
nautilus_kill_macosx_metadata_class_init (NautilusKillMacosxMetadataClass *nautilus_kill_macosx_metadata_class,
                                          gpointer                         class_data)
{
    parent_class = g_type_class_peek_parent (nautilus_kill_macosx_metadata_class);
}

void
nautilus_kill_macosx_metadata_register_type (GTypeModule *module)
{
    static const GTypeInfo info =
    {
        sizeof (NautilusKillMacosxMetadataClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) nautilus_kill_macosx_metadata_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        sizeof (NautilusKillMacosxMetadata),
        0,
        (GInstanceInitFunc) NULL,
        (GTypeValueTable *) NULL
    };

    nautilus_kill_macosx_metadata_type = g_type_module_register_type (
        module,
        G_TYPE_OBJECT,
        "NautilusKillMacosxMetadata",
        &info,
        0
        );

    static const GInterfaceInfo type_info_provider_iface_info =
    {
        (GInterfaceInitFunc) nautilus_kill_macosx_metadata_type_info_provider_iface_init,
        (GInterfaceFinalizeFunc) NULL,
        NULL
    };

    g_type_module_add_interface (
        module,
        nautilus_kill_macosx_metadata_type,
        NAUTILUS_TYPE_INFO_PROVIDER,
        &type_info_provider_iface_info
        );
}

GType
nautilus_kill_macosx_metadata_get_type (void)
{
    return nautilus_kill_macosx_metadata_type;
}

