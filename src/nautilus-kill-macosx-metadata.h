/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * This file is part of Nautilus Kill MacOSx Metadata.
 * https://gitlab.gnome.org/glerro/nautilus-kill-macosx-metadata
 *
 * nautilus-kill-macosx-metadata.h
 *
 * Copyright (c) 2022-2023 Gianni Lerro {glerro} ~ <glerro@pm.me>
 *
 * Nautilus Kill MacOSx Metadata is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Nautilus Kill MacOSx Metadata is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Nautilus Kill MacOSx Metadata. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022-2023 Gianni Lerro <glerro@pm.me>
 */

#pragma once

#ifndef NAUTILUS_KILL_MACOSX_METADATA_H
#define NAUTILUS_KILL_MACOSX_METADATA_H

#include <glib-object.h>

G_BEGIN_DECLS

#define NAUTILUS_KILL_MACOSX_METADATA_TYPE  (nautilus_kill_macosx_metadata_get_type ())
#define NAUTILUS_KILL_MACOSX_METADATA(o)    (G_TYPE_CHECK_INSTANCE_CAST ((o), NAUTILUS_KILL_MACOSX_METADATA_TYPE, NautilusKillMacosxMetadata))
#define NAUTILUS_KILL_MACOSX_METADATA_IS_NAUTILUS(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), NAUTILUS_KILL_MACOSX_METADATA_TYPE))

typedef struct _NautilusKillMacosxMetadata NautilusKillMacosxMetadata;
typedef struct _NautilusKillMacosxMetadataClass NautilusKillMacosxMetadataClass;

struct _NautilusKillMacosxMetadata
{
    GObject __parent;
};

struct _NautilusKillMacosxMetadataClass
{
    GObjectClass __parent;
};

void  nautilus_kill_macosx_metadata_register_type (GTypeModule *module);
GType nautilus_kill_macosx_metadata_get_type (void);

G_END_DECLS

#endif

