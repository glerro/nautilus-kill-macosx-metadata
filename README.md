# Nautilus Kill MacOSx Metadata
Nautilus Kill MacOSX Metadata is an extension for the file manager nautilus (aka File) that remove automaticaly
__MACOSX folder, ._.DS_Store and .DS_Store files.
The __MACOSX folder is created by the built in MacOSX zip utility, many files on the Mac have two parts: a data fork, and a resource fork, the __MACOSX folder contain the resource forks.
The file .DS_Store and ._.DS_Store (Desktop Services Store) is created in any directory accessed by the MacOS Finder application, and stores custom attributes of its containing folder, such as folder view options, icon positions, and other visual information.
For more information read this [AppleSingle and AppleDouble formats Wikipedia page](https://en.wikipedia.org/wiki/AppleSingle_and_AppleDouble_formats) and [.DS_Store Wikipedia page](https://en.wikipedia.org/wiki/.DS_Store).

## Dependencies
- `glib-2.0 >= 2.44`
- `libnautilus-extension ≥ 2.12.0` or `libnautilus-extension-4 ≥ 43`

## Building and Installing
You can check out the latest version with git, build and install it with [Meson](https://mesonbuild.com/).

In order to build you will need to have istalled gcc, git, meson and ninja.

For a regular use these are the steps:

```bash
git clone https://gitlab.gnome.org/glerro/nautilus-kill-mac-osx-metadata.git
cd nautilus-kill-macosx-metadata
meson setup --prefix=/usr _build .
meson compile -C _build
meson install -C _build
```

## License
Nautilus Kill MacOSx Metadata - Copyright (c) 2022-2023 Gianni Lerro {glerro} ~ <glerro@pm.me>

Nautilus Kill MacOSx Metadata is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Nautilus Kill MacOSx Metadata is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Nautilus Kill MacOSx Metadata. If not, see <https://www.gnu.org/licenses/>.

